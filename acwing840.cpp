#include<iostream>
#include<cstring>

using namespace std;

const int N = 100003; // N的整数最大为10e5  

//开链寻址法

int h[N],e[N],ne[N],idx; //数组 链表  指针 数组索引  



void insert(int x)
{
    int k = ((x%N)+N)%N; //桶下标
    e[idx] = x;
    ne[idx] = h[k];
    h[k] = idx++; // h相当于桶指针 指向e链表的地址
}

bool find(int x)
{
    int k = ((x%N)+N)%N;
    for(int i = h[k];i!=-1;i = ne[i]) //模拟链表循环遍历
    {
        if(e[i] == x)
        return true;
    }
    return false;
}


int main()
{
    int n;
    scanf("%d",&n);
    
    memset(h,-1,sizeof(h));
  //  memset(ne,-1,sizeof(h));
    while(n--)
    {
        char op[2];
        int v;
        scanf("%s%d",op,&v);//最好开一个字符串  如果用字符输入输出的话 会出现预期之外的情况
        if(*op == 'I')
        {
            insert(v);
        }else
        {
            if(find(v))
            puts("Yes");
            else
            puts("No");
        }
    }
    return 0;
}