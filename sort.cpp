#include<bits/stdc++.h>

using namespace std;


void quick_sort(vector<int> &ans,int l,int r)
{
    if(l>r)return ;
    int i = l;
    int j = r;
    int pos = ans[i];
    while(j>i)
    {
        while(pos>ans[i])i++;
        while(pos<ans[j])j--;
        if(j>i)
        {
            int temp = ans[i];
            ans[i] = ans[j];
            ans[j] = temp;
        } 
    }
    ans[i] = pos;
    quick_sort(ans,l,j-1);
    quick_sort(ans,j+1,r);
}

int main()
{
    vector<int> ans{1,5,2,56,7,67,8};
    quick_sort(ans,0,ans.size()-1);
    for(auto e:ans)
    cout<<e<<" ";
    return 0;
}