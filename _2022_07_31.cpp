#include<iostream>
#include<queue>
using namespace std;

  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};
 
class Solution {
public:
    int maxLevelSum(TreeNode* root) {
        if(!root) return 0;
        int index = 1,sum = root->val;
        queue<TreeNode*> que;
        que.push(root);
        int i = 1;
        while(!que.empty())
        {

            int s = que.size();
            int count = 0;
            for(int i = 0;i<s;i++)
            {
                auto it = que.front();
                que.pop();
                count+=it->val;
                if(it->left)que.push(it->left);
                if(it->right)que.push(it->right);
            }
            if(count>sum)
            {
                index = i;
                sum = count;
            }
            i++;
        }
        return index;
    }
};

int main()
{
    return 0;
}