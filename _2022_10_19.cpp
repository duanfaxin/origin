#include<iostream>
#include<vector>


using namespace std;
class Solution {
public:
    int countStudents(vector<int>& students, vector<int>& sandwiches) {
        int left = 0; //代表到三文治的idx
        int ans[2];
        for(auto e:students)
        {
            if(e == 0)ans[0]++;
            else ans[1]++;
        }
        while(left<sandwiches.size())
        {
            if(ans[sandwiches[left]] == 0)
            return sandwiches.size()-left;
            ans[sandwiches[left++]]--;
        }
        return 0;
    }
};


int main()
{
    return 0;
}