#include<bits/stdc++.h>

using namespace std;


class Solution {
public:
    int findLongestChain(vector<vector<int>>& pairs) {
        int n = pairs.size();
        vector<int> dp(n+1,1);
        sort(pairs.begin(),pairs.end());
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<i;j++)
            {
                if(pairs[i][0]>pairs[j][1])
                {
                    dp[i] = max(dp[i],dp[j]+1);
                }
            }
        }
        return dp[n-1];
    }
};


  struct TreeNode {
      int val;
    TreeNode *left;
    TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

class Solution {
public:
    int res;
    int longestUnivaluePath(TreeNode* root) {
        res = 0;
        dfs(root);
        return res;
    }
    int dfs(TreeNode* root)
    {
        if(!root)return 0;
        int l = dfs(root->left),r = dfs(root->right);
        int le = 0,ri = 0;
        if(root->left&&root->left->val == root->val)
        le = l + 1;
        if(root->right&&root->right->val == root->val)
        ri = r + 1;
        res = max(res,le + ri);
        return max(le,ri);
    }
};

// class Solution {
// private:
//     int res;

// public:
//     int longestUnivaluePath(TreeNode* root) {
//         res = 0;
//         dfs(root);
//         return res;
//     }

//     int dfs(TreeNode *root) {
//         if (root == nullptr) {
//             return 0;
//         }
//         int left = dfs(root->left), right = dfs(root->right);
//         int left1 = 0, right1 = 0;
//         if (root->left && root->left->val == root->val) {
//             left1 = left + 1;
//         }
//         if (root->right && root->right->val == root->val) {
//             right1 = right + 1;
//         }
//         res = max(res, left1 + right1);
//         return max(left1, right1);
//     }
// };


int main()
{


    return 0;
}