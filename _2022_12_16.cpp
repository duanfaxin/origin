#include<iostream>
#include<vector>

#include<math.h>
using namespace std;



class Solution {
public:
    int minElements(vector<int>& nums, int limit, int goal) {
        long long sum = 0;
        for(auto &e:nums)
        {
            sum+=e;
        }
        long long diff = abs(sum-goal);
        return (diff+limit-1)/limit;
    }
};

// class Solution {
// public:
//     int minElements(vector<int>& nums, int limit, int goal) {
//         long long sum = 0;
//         for(auto &e:nums)
//         sum+=e;
//         int count = 0;
//         while(sum!=goal)
//         {
//             if(sum == goal)
//             return count;
//             else if(sum>goal)
//             {
//                 if(sum-goal<=limit)return count +1;
//                 else{
//                     sum-=limit;
//                     count++;
//                 }
//             }else{
//                 if(goal-sum<=limit)return count+1;
//                 else{
//                     sum+=limit;
//                     count++;
//                 }
//             }
//         }
//         return -1;
//     }
// };
// class Solution {
// public:
//     int minElements(vector<int>& nums, int limit, int goal) {
//         long long sum = 0;
//         for (auto x : nums) {
//             sum += x;
//         }
//         long long diff = abs(sum - goal);
//         return (diff + limit - 1) / limit;
//     }
// };



int main()
{
    return 0;
}