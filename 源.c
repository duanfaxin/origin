#include<stdio.h>
#include "game.h"
#define _CRT_SECURE_NO_WARNINGS 1
#define ROW 3
#define COL 3
void InitBoard(char board[ROW][COL], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}
void ShowBoard(char board[ROW][COL], int row, int col)
{
	printf("=====================\n");
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			//三个空格
			printf(" %c ", board[i][j]);
			//两列竖线
			if (j < col - 1)
			{
				printf("|");
			}
		}
		//换行
		printf("\n");
		//横线只有2行
		if (i < row - 1)
		{
			for (int j = 0; j < col; j++)
			{
				printf("---");
				//
				if (j < col - 1)
				{
					printf("|");
				}
			}
			printf("\n");
		}

	}
	printf("=====================\n");
}
void playermove(char board[ROW][COL], int row, int col)
{
	while (1)
	{
		printf("请输入你的坐标：\n");
		int x = 0;
		int y = 0;
		scanf_s("%d%d", &x,&y);
		if (x >= 1 && x <= 3 && y >= 1 && y <= 3)
		{
			//x   o
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = 'x';
				break;
			}
			else
			{
				printf("该位置已经有棋子\n");
			}
		}
		else
		{
			printf("坐标不合法\n");
		}
	}
}
void ComputerMove(char board[ROW][COL], int row, int col)
{
	while (1)
	{
		int x = rand()%row;//[0,1,2]
		int y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = 'O';
			break;    //break退出while循环
		}
	}
}
static int IsFull(char board[ROW][COL], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				return -1;//没有满
			}
		}
	}
	return 1;//说明当前平局
}
char IsWin(char board[ROW][COL], int row, int col)
{
	//行
	for (int i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2]
			&& board[i][0] != ' ')
		{
			return board[i][0];
		}
	}

	//列
	for (int j = 0; j < col; j++)
	{
		if (board[0][j] == board[1][j] && board[1][j] == board[2][j]
			&& board[0][j] != ' ')
		{
			return board[0][j];
		}
	}

	if (board[0][0] == board[1][1] && board[1][1] == board[2][2]
		&& board[0][0] != ' ')
	{
		return board[0][0];
	}

	if (board[0][2] == board[1][1] && board[1][1] == board[2][0]
		&& board[0][2] != ' ')
	{
		return board[0][2];
	}

	//是否是平局
	if (IsFull(board, row, col) == 1)
	{
		return 'Q';//平局
	}

	return ' ';//没有平局  正常的情况下
}