  #include<bits/stdc++.h>
  using namespace std;
  
   vector<vector<bool>> bb(201,vector<bool>(201)); 
class Solution {
    //bool st[N][N];//记录是否遍历过
     const int N = 210;
   
public:
    int dx[4]  = {0,1,0,-1},dy[4] = {1,0,-1,0};
    int x,y; //记录当前位置
    
    bool dfs(int a,int b,vector<vector<char>>& board, string word,string temp)
    {
            if(temp == word)
            return true;
            //string ans = temp;
            for(int i = 0;i<4;i++)
            {   
                int _x = a + dx[i];
             int _y = b + dy[i];
                if(_x>=0&&x<board.size()&&_y>=0&&_y<board[0].size()&&!bb[_x][_y])
                {
                    bb[_x][_y] = true;
                    temp.push_back(board[_x][_y]);
                    dfs(_x,_y,board,word,temp);
                    temp.pop_back();
                    bb[_x][_y] = false;
                }
            }
            return false;
    }
    bool exist(vector<vector<char>>& board, string word) {
        //深搜
        int n = board.size();
        if(n == 0)return false;
        int m = board[0].size();
        string temp = "";
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<m;j++)
            {
               if(dfs(i,j,board,word,temp)) 
               return true;
               bb.clear();
               temp.clear();
            }
        }
        return false;
    }
};
int main()
{
    return 0;
}