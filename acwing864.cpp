#include<iostream>
#include<algorithm>
#include <cstring>
using namespace std;

const int N = 100010;

int h[N],e[N],ne[N]; //h[N]是标识  相当于idx  e[N]存数值  ne[N]相当于指针
bool st[N];
int idx = 0;
int ans = N;
 int n;
void add(int a,int b)
{
    e[idx] = a,ne[idx] = h[b],h[b] = idx++;
}
int dfs(int u)
{
    st[u] = true;
    int sum = 1;
    int res = 0;
    for(int i = h[u];i!=-1;i = ne[i])
    {
        int j = e[i];
        if(!st[j])
        {
          int s =  dfs(j);
          res = max(res,s);
          sum+=res;
        }
    }
    res = max(res,n-sum);
    ans = min(res,ans);
    return sum;
}
int main()
{
    memset(h,-1,sizeof(h));
    
   
    cin>>n;
    for(int i = 0;i<n-1;i++)
    {
        int a,b;
        cin>>a>>b;
        add(a,b);
        add(b,a);
    }
    dfs(1);
    cout<<ans<<endl;
    return 0;
}