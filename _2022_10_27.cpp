#include<iostream>
#include<string>
#include<vector>

using namespace std;
class Solution {
public:
    bool isPalindrome(int x) {
        string s = to_string(x);
        int right = s.size()-1;
        int left = 0;
        while(right>left)
        {
            if(s[right--]!=s[left++])
            return false;
        }
        return true;
    }
};

class Solution {
public:
    int arraySign(vector<int>& nums) {
        int a = 1;
        for(auto &e : nums)
        {
            if(e == 0)
            return 0;
            if(e<0)
            a = -a;
        }
        return a;
    }
};

int main()
{
    return 0;
}