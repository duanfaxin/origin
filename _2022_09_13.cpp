#include<iostream>
#include<bits/stdc++.h>

#include<algorithm>

using namespace std;


class Solution {
public:
    int maximumSwap(int num) {
        vector<int> ans;
        while(num>0)
        {
            int t = num%10;
            ans.push_back(t);
            num/=10;
        }

        vector<int> res = ans;
        sort(res.begin(),res.end());
        for(int i = res.size()-1;i>=0;i--)
        {
            if(ans[i] != res[i])
            {
                int b = ans[i];
                int t = res[i];
                ans[i] = res[i];
                i--;
                while(i>=0)
                {
                    if(t == ans[i])
                    {
                        ans[i] = b;
                        break;
                    }
                    i--;
                }
                break;
            }
        }
        int count = 0;
        for(int i = ans.size()-1;i>=0;i--)
        {
            count*=10;
            count+=ans[i];
        }
        return count;
    }
};

int main()
{
    return 0;
}