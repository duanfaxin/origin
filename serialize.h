#pragma once
#include <vector>
#include <string>
#include <list>
#include <map>
#include <cstring>
#include <iostream>
#include <set>

using namespace std;
namespace xdyun
{
    namespace serialize
    {

        class DataStream
        {
        public:
            enum DataType //枚举类型
            {
                BOOL = 0,
                CHAR,
                INT32,
                INT64,
                FLOAT,
                DOUBLE,
                STRING,
                VECTOR,
                LIST,
                MAP,
                SET,
                CUSTOM
            };
            DataStream(){}
            ~DataStream(){}
            void write(const char *data, int len);
            void write(bool value);
            void write(int32_t value);
            void write(int64_t value);
            void write(float value);
            void write(double value);
            void write(const char *value);
            void write(const string &value);
            template <class T>
            void write(const vector<T> value);
            void show();

        private:
            std::vector<char> m_buf; //存储序列化数据
            void reserve(int len);
        };
        void DataStream::show()
        {
            int size = m_buf.size();
            cout << "data size = " << size << endl;
            int i = 0;
            while (i < size)
            {
                switch ((DataType)m_buf[i])
                {
                case DataType::BOOL:
                    if (m_buf[++i] == 0)
                        cout << "false" << endl;
                    else
                        puts("true");
                    i++;
                case DataStream::CHAR:
                    cout<<m_buf[++i]<<endl;
                    i++;
                    break;
                case DataType::INT32:
                    cout<<*((int32_t*)(&m_buf[++i]))<<endl;
                    i+=4;
                    break;
                case DataType::INT64:
                    cout<<*((int64_t*)(&m_buf[++i]))<<endl;
                    i+=8;
                    break;
                case DataType::FLOAT:
                    cout<<*((float*)(&m_buf[++i]))<<endl;
                    i+=4;
                    break;
                case DataType::DOUBLE:
                    cout<<*((double*)(&m_buf[++i]))<<endl;
                    i+=8;
                    break;
                case DataType::STRING:
                    if(((DataType)m_buf[++i]) == DataType::INT32)
                    {   
                        int32_t len = *((int*)(&m_buf[++i]));
                        i+=4;
                        cout<<string(&m_buf[i],len)<<endl;
                        i+=len;
                    }else{
                        throw std::logic_error("parse string error!");
                    }
                default:
                    break;
                }
            }
        }
        void DataStream::reserve(int len)
        {
            int size = m_buf.size();
            int cap = m_buf.capacity();
            if (len + size > cap)
            {
                while (len + size > cap)
                {
                    if (cap == 0)
                        cap = 1;
                    else
                        cap *= 2;
                }
            }
            m_buf.reserve(cap); //保证足够的空间容纳数据
        }
        // memcpy字节拷贝，根据字节来拷贝，str遇到'/0'结束拷贝
        void DataStream::write(const char *data, int len)
        {
            reserve(len);
            int size = m_buf.size();
            m_buf.resize(len + size);
            std::memcpy(&m_buf[size], data, len);
        }
        void DataStream::write(bool value)
        {
            char type = DataType::BOOL;
            write((char *)&type, sizeof(char));  //先写类型
            write((char *)&value, sizeof(char)); //数据
        }
        void DataStream::write(int32_t value)
        {
            char type = DataType::INT32;
            write((char *)&type, sizeof(char)); //先写类型
            write((char *)&value, sizeof(int32_t));
        }
        void DataStream::write(int64_t value)
        {
            char type = DataType::INT64;
            write((char *)&type, sizeof(char)); //先写类型
            write((char *)&value, sizeof(int64_t));
        }
        void DataStream::write(float value)
        {
            char type = DataType::FLOAT;
            write((char *)&type, sizeof(char)); //先写类型
            write((char *)&value, sizeof(float));
        }
        void DataStream::write(double value)
        {
            char type = DataType::DOUBLE;
            write((char *)&type, sizeof(char)); //先写类型
            write((char *)&value, sizeof(double));
        }
        void DataStream::write(const char *value)
        {
            char type = DataType::STRING;
            write((char *)&type, sizeof(char)); // 1
            int len = strlen(value);
            write(len); // 5 直接用了int32_t中的write
            write(value, len);
        }
        void DataStream::write(const string &value)
        {
            char type = DataType::STRING;
            write((char *)&type, sizeof(char));
            int len = value.size();
            write(len);
            write(value.data(), len);
        }
    }

}