#include<iostream>

using namespace std;
//饿汉模式  本身线程安全
class solo{

public:
    static solo* get_instrance();
private:
    solo(){}
    solo(solo &ss){}
    ~solo(){}
private:
    static solo* s;
};
    solo* solo::get_instrance()
    {
        return s;
    }
    solo* solo::s = new solo();
int main()
{
    return 0;
}