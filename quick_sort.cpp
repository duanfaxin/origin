#include<iostream>
#include<vector>
using namespace std;
template<class T>
void quick_sort(vector<T> &nums,int left,int right)
{
    if(right<=left) return ;
    T pos = nums[left];
    int l = left-1;
    int r = right+1;
    while(l<r)
    {
        while(pos>nums[++l]);
        while(pos<nums[--r]);
        if(l<r)swap(nums[l],nums[r]);
    }
    quick_sort(nums,left,r),quick_sort(nums,r+1,right);
}
int main()
{
    vector<int> nums = {12,45,448,61,2,3,4,5,6,7};
    quick_sort(nums,0,nums.size()-1);
    for(auto e: nums)
    cout<<e<<endl;
    return 0;
}