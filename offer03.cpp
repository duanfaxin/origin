#include<iostream>
#include<vector>

using namespace  std;

class Solution {
public:
    int maxValue(vector<vector<int>>& grid) {
        //动态规划
        int n = grid.size();
        int m = grid[0].size();
        vector<vector<int>> dp(n+1,vector<int>(m+1,0));
        dp[0][0] = grid[0][0];
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<m;j++)
            {
                if(i == 0&&j!=0)
                dp[i][j] = grid[i][j] + dp[i][j-1];
                if(i!=0&&j==0)
                dp[i][j] = grid[i][j] + dp[i-1][j];
                if(i>0&&j>0)
                dp[i][j] = max(dp[i-1][j],dp[i][j-1]) + grid[i][j];
            }
        }
        return dp[n-1][m-1];
    }
};


int main()
{
    return 0;
}