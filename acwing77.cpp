#include<vector>
#include<string>
using namespace std;
class Solution {
public:
    string reverseWords(string s) {
        if(s.size() == 0)
        return s;
        vector<string> ve;
        int index = 0;
       // cout<<s.size();
      // string sss;
       while(index<s.size())
       {
           string t;
           while(s[index] != ' ')
           {
               t+=s[index];
               index++;
           }
           ve.push_back(t);
           index++;
       }
       string ans = "";
       for(int i = ve.size()-1;i>=0;i--)
       {
           ans+=ve[i];
           ans+=' ';
       }
       //cout<<ve.size();
       ans.pop_back();
       return ans;
    }
};

int main()
{
    return 0;
}