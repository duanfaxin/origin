#include<iostream>
#include<vector>
#include<queue>
#include<deque>
#include<algorithm>
using namespace std;

// class Solution {
// public:
//     int shortestSubarray(vector<int>& nums, int k) {
//         int summ = 0;
//         int count = INT_MAX;
//         int n = nums.size();
//         queue<int> que;
//         int i = 0;
//         while(i<n)
//         {
//             if(summ<k)
//             {
//                 que.push(nums[i]);
//                 summ+=nums[i];
//                 i++;
//             }
//                 if(summ>=k)
//                 {
//                 count = count>que.size()?que.size():count;
//                 //count = min(que.size(),count);
//                 que.pop();
//                 summ-=que.front();
//             }
//         }
//         if(summ<k) return -1;
//         return count;
//     }
// };

class Solution {
public:
    int shortestSubarray(vector<int>& nums, int k) {
        int n = nums.size();
        vector<long> preSumArr(n + 1);
        for (int i = 0; i < n; i++) {
            preSumArr[i + 1] = preSumArr[i] + nums[i];
        }
        int res = n + 1;
        deque<int> qu;
        for (int i = 0; i <= n; i++) {
            long curSum = preSumArr[i];
            while (!qu.empty() && curSum - preSumArr[qu.front()] >= k) {
                res = min(res, i - qu.front());
                qu.pop_front();
            }
            while (!qu.empty() && preSumArr[qu.back()] >= curSum) {
                qu.pop_back();
            }
            qu.push_back(i);
        }
        return res < n + 1 ? res : -1;
    }
};



int main()
{
    return 0;
}