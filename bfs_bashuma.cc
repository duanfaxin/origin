#include<iostream>
#include<string>
#include<unordered_map>
#include<queue>
#include<algorithm>

using namespace std;

int dx[4] = {-1,0,1,0},dy[4] = {0,1,0,-1};
queue<string>  que;  //将每一步关联起来

int bfs(string s)
{
    unordered_map<string,int> mp; //保存每一个状态对应的步数
    que.push(s);
    mp[s] = 0;
    string end = "12345678x";
    while(!que.empty())
    {
        auto t = que.front();   //将t拿出来 寻找与他相连的的状态
        que.pop();
        int distance = mp[t];  //  保存初识状态到t的步数
        if(t == end)return distance; //判断t是不是我们要找的
        int k = t.find('x');//找到x的位置
        int x = k/3,y = k%3;
        for(int i = 0;i<4;i++)  //寻找与他相连的的状态 换位
        {
            int xx = x + dx[i],yy = y + dy[i];
            if(xx>=0&&xx<3&&yy>=0&&yy<3)
            {
                swap(t[k],t[xx*3+yy]); //其中之一的状态  之后要恢复
                if(!mp.count(t))
                {
                    mp[t] = distance + 1;
                    que.push(t);
                }
                 swap(t[k],t[xx*3+yy]); //恢复
            }
        }
  
    }
    return -1;
}




int main()
{
    string s;
    for(int i =0;i<9;i++)
    {
        char c;  //因为输入格式  有空格 所以一个一个加
        cin>>c;
        s+=c;
    }
    
    cout<<bfs(s);
    
    return 0;
}
