#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

class Solution {
public:
    vector<string> ans;
    string temp;
    bool f[10];//默认是false
    void dfs(string s)
    {
        if(temp.size()==s.size())
        {
            auto it  =  find(ans.begin(),ans.end(),temp);
            if(it == ans.end())
            ans.push_back(temp);
            return ;
        }
        
        for(int i = 0;i<s.size();i++)
        {
            if(!f[i])
            {
                temp.push_back(s[i]);
                f[i] = true;
                dfs(s);
                f[i] = false;
                temp.pop_back();
            }
        }
    }
    vector<string> permutation(string s) {
        if(!s.size()) return ans;
        dfs(s);
        return ans;
            
    }
};


int main()
{
    return 0;
}