#include<iostream>
#include<stack>
using namespace std;

class MinStack {
public:
    /** initialize your data structure here. */
    MinStack() {
     //   st1.push(INT_MAX);
        //  st2.push(INT_MAX);

    }
    
    void push(int x) {
        st1.push(x);
        if(st2.empty())
        st2.push(x);
        else
        {
            if(st2.top()>=x)
            st2.push(x);
        }
    }
    
    void pop() {
       
        if(st1.top() == st2.top())
        st2.pop();
        st1.pop();
    }
    
    int top() {
        return st1.top();
    }
    
    int min() {
        return st2.top();
    }   
private:
    stack<int> st1;
    stack<int> st2;
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->min();
 */

int main()
{
    int p = 20;
    int* t = new int[p];
    for(int i = 0;i<p;i++)
    {
        t[i] = i;
        cout<<t[i];
    }
}