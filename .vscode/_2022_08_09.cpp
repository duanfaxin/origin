#include<iostream>
#include<vector>

using namespace std;

class Solution {
public:
    int minStartValue(vector<int>& nums) {
        vector<int> ans(nums.size());
        ans[0] = nums[0];
        int count  =ans[0];
        for(int i = 1;i<nums.size();i++)
        {
            ans[i] = ans[i-1] + nums[i];
            count = min(count,ans[i]);
        }
        if(count>=0) return 1;
        return 1-count;
    }
};


int main()
{
    return 0;
}