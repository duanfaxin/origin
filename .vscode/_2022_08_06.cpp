#include<iostream>

#include<vector>
#include<deque>
using namespace std;



class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
            //滑动窗口的最大值  单调队列
        vector<int> ans;
        deque<int> dq;
        for(int i = 0;i<nums.size();i++)
        {
            while(dq.size()&&i-k>=dq.front())dq.pop_front();
            while(dq.size()&&nums[dq.back()]<=nums[i])dq.pop_back();
            dq.push_back(i);
            if(i-k>=-1)
            ans.push_back(nums[dq.front()]);
        }
        return ans;
    }
};



int main()
{
    return 0;
}
