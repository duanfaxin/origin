#include<iostream>
#include<vector>
#include<stack>

using namespace std;


/*class Solution {
public:
    //bool comp(int a,)
    vector<int> exclusiveTime(int n, vector<string>& logs) {
        vector<int> st,ends;
        unordered_map<int,pair<int,int>> mp;
        for(auto &e:logs)
        {
            int p = e[0]-'0'+1;
            if(e.find('e')==string::npos)
            {
                if(mp.count(p)!=0)
                mp[p].first = e[8];
                else
                mp[p] = make_pair<int,int>(e[8],-1);
            }else{
                if(mp.count(p)!=0)
                mp[p].second = e[6];
                else
                mp[p]=make_pair<int,int>(-1,e[6]);
            }
        } //将所有的信息都存储到mp中
        int ss = mp[n].first;
        int ee = mp[n].second;
        unordered_set<int> ans;
        int tt = ss;
        for(int i =ss;i<=ee;i++)
        {
            ans.insert(ss);
        }
        for(auto e:mp)
        {
            if(e.second.first>ss)
            {
               for(int i = e.second.first;i<=e.second.second;i++) //将栈所占用的时间剔除掉
                ans.erase(i); 
            } 
        }
        vector<int> ret(ans.begin(),ans.end());
        return ret;
    }
};*/

class Solution {
public:
    vector<int> exclusiveTime(int n, vector<string>& logs) {
        stack<pair<int, int>> st; // {idx, 开始运行的时间}
        vector<int> res(n, 0);
        for (auto& log : logs) {
            char type[10];
            int idx, timestamp;
            sscanf(log.c_str(), "%d:%[^:]:%d", &idx, type, &timestamp);
            if (type[0] == 's') {
                if (!st.empty()) {
                    res[st.top().first] += timestamp - st.top().second;
                    st.top().second = timestamp;
                }
                st.emplace(idx, timestamp);
            } else {
                auto t = st.top();
                st.pop();
                res[t.first] += timestamp - t.second + 1;
                if (!st.empty()) {
                    st.top().second = timestamp + 1;
                }
            }
        }
        return res;
    }
};



int main()
{
    return 0;
}
