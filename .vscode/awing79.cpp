#include<iostream>
#include<bits/stdc++.h>
using namespace std;

class Solution {
public:
    vector<int> maxInWindows(vector<int>& nums, int k) {
        //单调栈
        //记录元素下标
        vector<int> res;
        deque<int> q;
        for(int i = 0;i<nums.size();i++)
        {
            if(q.size()&&i-q.front()>=k)q.pop_front();
            while(q.size()&&nums[q.back()]<=nums[i])q.pop_back();
            q.push_back(i);
            if(i>=k-1)
            res.push_back(nums[q.front()]);
        }
        return res;
    }
};


int main()
{
    return 0;
}