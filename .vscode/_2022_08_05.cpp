#include<iostream>
#include<queue>

using namespace std;


/**
  Definition for a binary tree node.*/
  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };
 
class Solution {
public:
    TreeNode* addOneRow(TreeNode* root, int val, int depth) {
        if(depth == 1)
        {
            TreeNode* hh = new  TreeNode(val);
            hh->left = root;
            return hh;
        }
        queue<TreeNode*> que;
        
        que.push(root);
        int d = 1;
        while(!que.empty())
        {
            if(d == depth-1)
            break;
            int s = que.size();
            for(int i = 0;i<s;i++)
            {
                auto it = que.front();
                que.pop();
                if(it->left)que.push(it->left);
                if(it->right)que.push(it->right);
            }
        }
        while(!que.empty())
        {
            auto it = que.front();
           TreeNode *cur = new TreeNode(val);
            if(it->left)
            { 
                cur->left = it->left;
            }
            it->left = cur; 
            TreeNode *cur1 = new TreeNode(val);
            if(it->right)
            {  
                cur1->right = it->right;             
            }  
            it->right = cur1;
            que.pop();
        }
        return root;
    }
};

int main()
{
    return 0;
}