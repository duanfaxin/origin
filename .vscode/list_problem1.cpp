#include<iostream>
using namespace std;


  struct ListNode {
      int val;
     ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
  };
 
class Solution {
public:
    ListNode* getKthFromEnd(ListNode* head, int k) {
        ListNode* fast = head;
        ListNode* slow = head;
        for(int i = 0;i<k;i++)
        {
            fast = fast->next;
        }
        while(fast)
        {
            slow = slow->next;
            fast = fast->next;
        }
        return slow;
    }
};

class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* hh = new ListNode(-1);
        ListNode* cur = head;
        while(head)
        {
            cur = head->next;
            head->next = hh->next;
            hh->next = head;
            head = cur;
        }
        return hh->next;
    }
};


class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        ListNode* pre = nullptr;
        ListNode* cur = head;
        while(head)
        {
            cur = head->next;
            head->next = pre;
            pre = head;
            head = cur;
        }
        return pre;
    }
};
int main()
{
    return 0;
}