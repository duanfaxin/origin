#include<iostream>
#include<vector>

using namespace std;


class Solution {
public:
    vector<vector<int> > findContinuousSequence(int sum) {
        vector<vector<int>> res;
        vector<int> path;
        for(int i = 1, j = 2; j < sum && i < j; j) {
            int ans = (i + j) * (j - i + 1) / 2;
            if ( ans == sum){//如果相同就加入。
                int k = i;
                while(k <= j)
                    path.push_back(k++);
                res.push_back(path);
                path.clear();
                i ++, j ++;//两个指针同时往后移。
            }
            else if ( ans < sum) {//如果比较小，j就往后移动。
                j ++;
            }
            else 
                i ++;//否则i往后移动
        }
        return res;

    }
};

int main()
{
    return 0;
}