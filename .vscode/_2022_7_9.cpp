#include<iostream>
#include<vector>
#include<queue>
#include<unordered_map>

using namespace std;

 struct TreeNode {
     int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
  };

class Solution {
public:
      unordered_map<int,int> mp;
    TreeNode* _buildTree(vector<int>& preorder,int pl,int pr, vector<int>& inorder,int il,int ir)
    {
        if(pl>pr)return nullptr;
        TreeNode* root = new TreeNode(preorder[pl]);
        int t = mp[preorder[pl]];
        root->left = _buildTree(preorder,pl+1,pl+t-il,inorder,il,t-1);
        root->right = _buildTree(preorder,pl+t-il+1,pr,inorder,t+1,ir);
        return root;
    }
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        int n = preorder.size();
        if(n == 0)return NULL;
        for(int i = 0;i<n;i++)
        {
            mp[inorder[i]] = i;//将中序遍历存进map中  
        }
        return  _buildTree(preorder,0,preorder.size()-1,inorder,0,inorder.size()-1);
    }
};
//   struct TreeNode {
//       int val;
//       TreeNode *left;
//       TreeNode *right;
//      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
//   };
 
 /**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    bool _isSymmetric(TreeNode* _left,TreeNode* _right)
    {
        if(!_left&&!_right)return true;
        if(!_right||!_left)return false;
        if(_left->val!=_right->val) return false;
        return _isSymmetric(_left->left,_right->right)&&_isSymmetric(_left->right,_right->left);
    }
    bool isSymmetric(TreeNode* root) {
        if(!root) return true;
        return _isSymmetric(root->left,root->right);
    }
};
class Solution {
public:
    vector<int> levelOrder(TreeNode* root) {
        vector<int> ans;
        if(root == nullptr) return ans; 
        queue<TreeNode*> que;
        que.push(root);
        while(!que.empty())
        {
            auto t = que.front();
            que.pop();
            ans.push_back(t->val);
            if(t->left)que.push(t->left);
            if(t->right)que.push(t->right);
        }
        return ans;
    }
};
int main()
{
    return 0;
}