#include<iostream>
#include<algorithm>
#include<vector>
#include<string>
using namespace std;

class Solution {
public:
    string customSortString(string order, string s) {
        //自定义排序
        vector<int> val;
        val.resize(26);
        for(int i = 0;i<order.size();i++)
        {
            val[order[i]-'a'] = i+1;//给每一点赋予权重
        }
        sort(s.begin(),s.end(),[&](char c1,char c2){
            return val[c1-'a']<val[c2-'a'];
        });
        return s;
    }
};


int main()
{
    return 0;
}