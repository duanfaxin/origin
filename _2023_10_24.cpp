#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

class Solution {
public:
    int partitionDisjoint(vector<int>& nums) {
        int n = nums.size();
        vector<int> minright(n);
        minright[n-1] = nums[n-1];//初始
        for(int i = n-2;i>=0;i--){
            minright[i] = min(nums[i],minright[i+1]);
        }
        int maxleft = 0;
        for(int i = 0;i<n-1;i++)
        {
            maxleft = max(nums[i],maxleft);
            if(maxleft<=minright[i+1])
            return i+1;
        }
        return n-1;
    }
};


int main()
{
    return 0;
}