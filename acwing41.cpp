#include<iostream>
#include<bits/stdc++.h>
using namespace std;
 

class MinStack {
public:
    /** initialize your data structure here. */
    stack<int> st,minst;
    MinStack() {
    }
    
    void push(int x) {
       if(minst.empty())
       minst.push(x);
       else
       {
           if(minst.top()>=x)
           minst.push(x);
       }
       st.push(x);
    }
    
    void pop() {
        if(minst.top() == st.top())
        minst.pop();
        st.pop();
    }
    
    int top() {
        if(!st.empty())
        return st.top();
        return -1;
    }
    
    int getMin() {
        if(!minst.empty())
        return minst.top();
        return -1;
    }

};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */
int main()
{
    return 0;
}