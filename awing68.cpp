#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int getMissingNumber(vector<int>& nums) {
        //经典二分问题，将数组分成两段  前一段为数值等于索引，后一段为数值等于索引加一
        int n = nums.size();
        int l = 0,r = n;
        while(r>l)
        {
            int mid = r+l>>1;
            if(nums[mid] == mid)l = mid + 1;
            else r = mid;
        }
        return l;
    }
};

int main()
{
    return 0;
}