#include<iostream>
#include<vector>

using namespace std;

 //分治的思想
 
class Solution {
public:
    bool Postorder(vector<int>& postorder,int start,int end)
    {
        if(start>end) return true;
        int index = start;
        while(postorder[index]<postorder[end])index++;
        int mid = index;
        while(postorder[index]>postorder[end])index++;
        bool left = Postorder(postorder,start,mid-1);
        bool right = Postorder(postorder,mid,end-1);
        return index==end&&left&&right;
    }
    bool verifyPostorder(vector<int>& postorder) {
        return Postorder(postorder,0,postorder.size()-1);
    }
};


///dfs

  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
      TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };
 
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> temp;
    bool f = true;
    void dfs(TreeNode* root,int target,int count)
    {
        if(!root) return ;
        temp.push_back(root->val);
        count+=root->val;
        if(!root->left&&!root->right&&count == target)
        ans.push_back(temp);
        dfs(root->left,target,count);
        dfs(root->right,target,count);
        temp.pop_back();
    }
    vector<vector<int>> pathSum(TreeNode* root, int target) {
        if(!root)return ans;
        dfs(root,target,0);
        return ans;
    }
};
int main()
{
    return 0;
}