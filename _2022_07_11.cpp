#include<iostream>
using namespace std;


  struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode() : val(0), left(nullptr), right(nullptr) {}
     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
      TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
  };

class Solution {
public:
    //TreeNode* hh = new TreeNode(-1);
    TreeNode* _mergeTrees(TreeNode* root1,TreeNode* root2)
    {
        if(!root2&&!root1) return nullptr;
        TreeNode* hh;
        if(root1&&root2)
        {
            hh = new TreeNode(root1->val+root2->val);
            hh->left = _mergeTrees(root1->left,root2->left);
            hh->right = _mergeTrees(root1->right,root2->right);
        } 
        if(!root1&&root2)
        {
             hh = new TreeNode(root2->val);
             hh->left = root2->left;
             root2->left = nullptr;
             hh->right = root2->right;
             root2->right = nullptr;
        }
        if(root1&&!root2)
        {
             hh = new TreeNode(root1->val);
             hh->left = root1->left;
             root1->left = nullptr;
             hh->right = root1->right;
             root1->right = nullptr;
        }
        return hh;
    }
    TreeNode* mergeTrees(TreeNode* root1,TreeNode* root2)
    {
        return _mergeTrees(root1,root2);
    }
};

int main()
{
    return  0;
}