#include<iostream>

#include<bits/stdc++.h>


using namespace std;

class Solution {
public:
    int countMatches(vector<vector<string>>& items, string ruleKey, string ruleValue) {
        unordered_map<string,int> mp;
        mp["name"] = 2;
        mp["color"] = 1;
        mp["type"] = 0;
        int count = 0;
        for(auto &e:items)
        {
            if(e[mp[ruleKey]] == ruleValue)
                count++;
        }
        return count;
    }
};


int main()
{
    return 0;
}