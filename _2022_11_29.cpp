#include<iostream>


using namespace std;


class Solution {
public:
    int minOperations(string s) {
        int count = 0;
        for(int i = 0;i<s.size();i++)
        {
            char c = s[i];
            if(c == '0'+(i%2))
            {
                count++;
            }
        }
        return min(count,(int)s.size()-count);
    }
};


int main()
{
    return 0;
}