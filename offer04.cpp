#include<iostream>
#include<bits/stdc++.h>
using namespace std;

#include<algorithm>
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_set<char> st;
        queue<char> ss;
        int maxx = 0;
        for(int i = 0;i<s.size();i++)
        {
            if(!st.count(s[i]))
            {
               // if(ss.empty()||ss.front() != s[i])
               ss.push(s[i]);
                 st.insert(s[i]);
            }else
            {
                //st.erase(s[i]);
                while(ss.front()!=s[i])
                {
                    st.erase(ss.front());
                    ss.pop();
                }
                ss.pop();
                ss.push(s[i]);
            }
            maxx = maxx>ss.size()?maxx:ss.size();
        }
        return maxx;
    }
};


int main()
{
    return 0;
}