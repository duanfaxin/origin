#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;


class Solution {
public:
    int maxArea(vector<int>& height) {
        int l = 0;
        int r = height.size()-1;
        //int ml = 0,mr = 0;
        int maxx = 0;
        while(r>l)
        {
            int minn = min(height[l],height[r]);
            maxx = max(maxx,minn*(r-l));
            if(height[l]>height[r])
            {
                r--;
            }else
            {
                l++;
            }
        }
        return maxx;
    }
};

class Solution {
public:
    int jump(vector<int>& nums) {
        int maxPos = 0, n = nums.size(), end = 0, step = 0;
        for (int i = 0; i < n - 1; ++i) {
            if (maxPos >= i) {
                maxPos = max(maxPos, i + nums[i]); //找到最远位置
                if (i == end) {
                    end = maxPos;
                    ++step;
                }
            }
        }
        return step;
    }
};

class Solution {
public:
   // int a[310][310]; 
    int dx[4] = {1,0,-1,0},dy[4] = {0,1,0,-1};
    void ranse(vector<vector<char>>& grid,int i,int j)
    {
        if(i<0||j<0||i>=grid.size()||j>=grid[0].size()||grid[i][j] != '1')
            return ;     
            grid[i][j] = 0;
          //  a[i][j] = true;
        for(int x = 0;x<4;x++)
        {
            ranse(grid,i+dx[x],j+dy[x]);
        }
    }
    int numIslands(vector<vector<char>>& grid) {
        int n = grid.size();
        int m = grid[0].size();
        int count = 0;
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<m;j++)
            {
                if(grid[i][j] == '1')
                {
                    ranse(grid,i,j);
                    count++;
                //    cout<<"ni";
                //    a[i][j] = true;
                }
            }
        }
        return count;
    }
};
class Solution {
public:
    int findContentChildren(vector<int>& g, vector<int>& s) {
        sort(s.begin(),s.end());
        sort(g.begin(),g.end());
        int count = 0;
        int index = 0;
        for(int i = 0;i<g.size();i++)
        {
            while(index<s.size()&&s[index]<g[i])
            {
                index++;
            }
            if(index<s.size()&&s[index]>=g[i])
            {
                count++;
                index++;
            }
            if(index>=s.size())
            return count;
        }
        return count;
    }
};

int main()
{
    return 0;
}