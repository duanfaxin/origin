#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int numSubarrayBoundedMax(vector<int>& nums, int left, int right) {
        int len =  nums.size();
        int count = 0;
        int sum = 0;
        int l = 0;
        int maxx = 0;
        while(l<len)
        {
            if(nums[l]<=right&&nums[l]>=left)
            {
                count++;
                l++;
            }else
            {
                l++;
                sum+=(count*(count+1))/2;
                count = 0;
            }
        }
        return sum;
    }
};


int main()
{
    return 0;
}