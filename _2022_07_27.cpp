#include<iostream>
#include<queue>
using namespace std;


 // Definition for a binary tree node.
 struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

class Solution {
    int abs( const int a,const int b)
    {
        if(a>b)return a-b;
        return b-a;
    }
public:
    int _isBalanced(TreeNode* root)
    {
        if(root == nullptr) return 0;
        return max(_isBalanced(root->left),_isBalanced(root->right))+1;
    }
    bool dfs(TreeNode* root)
    {
        if(!root) return true;
        if(abs(_isBalanced(root->left),_isBalanced(root->right))>1) return false;
        return dfs(root->left)&&dfs(root->right);
    }
    bool isBalanced(TreeNode* root) {
        return dfs(root);
    }
};


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    int maxDepth(TreeNode* root) {
        if(root == NULL)return 0;
        int maxx = 0;
        //层次遍历
        queue<TreeNode*> que;
        que.push(root);
        while(!que.empty())
        {
            int s = que.size();
            for(int i = 0;i<s;i++)
            {
                auto it = que.front();
                que.pop();
                if(it->left)que.push(it->left);
                if(it->right)que.push(it->right);
            }
            maxx++;
        }
        return maxx;
    }
};


int main()
{
    return 0;
}