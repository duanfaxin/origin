#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

class Solution {
public:
    int translateNum(int num) {
       // int n = num.size();
        vector<int> temp;
        while(num>0)
        {
            temp.push_back(num%10); //用temp数组保存num每一位数字
            num/=10;
        }
        reverse(temp.begin(),temp.end());
        int n = temp.size();
        if(n<2)return 1;
        vector<int> dp(n+1);
        dp[0] = 1; //初始状态
        dp[1] = 1;
        for(int i = 2;i<=n;i++)
        {
            if(temp[i-2]!=0&&temp[i-2]*10+temp[i-1]<=25)
            dp[i] = dp[i-1]+dp[i-2];
            else
            dp[i] = dp[i-1];
        }
        return dp[n];
    }
};



int main()
{
    return 0;
}