#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    int findMin(vector<int>& nums) {
        if(nums.size() ==0)
        return -1;
        int n = nums.size()-1;
        while(n>0&&nums[n] == nums[0])n--;
        if(nums[n]>=nums[0])return nums[0];
        int r = n,l = 0;
        while(r>l)
        {
            int mid = r+l>>1;
            if(nums[mid]<nums[0])r = mid;
            else
            l = mid +1;
        }
        return nums[r];
    }
};

int main()
{
    return 0;
}