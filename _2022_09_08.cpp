#include<bits/stdc++.h>

using namespace std;

// class Solution {
// public:
//     int longestValidParentheses(string s) {
//         int index = 0;
//         int len = s.size();
//         stack<char> st;
//         int maxx = 0;
//         int count = 0;
//         while(index<len)
//         {
//             if(st.empty()&&s[index] == '(')
//             {
//                 st.push(s[index]);
//                 index++;
//             }else if(!st.empty()&&s[index] == ')')
//             {
//                 count+=2;
//                 maxx = max(count,maxx);
//                 st.pop();
//                 index++;
//             }else if(!st.empty()&&s[index] == '(')
//             {
//                 st.push(s[index]);
//                 index++;
//             }else{
//                 index++;
//                 count = 0;
//             }
//         }
//         return maxx;
//     }
// };
class Solution {
public:
    int longestValidParentheses(string s) {
        int maxans = 0;
        stack<int> stk;
        stk.push(-1);
        for (int i = 0; i < s.length(); i++) {
            if (s[i] == '(') {
                stk.push(i);
            } else {
                stk.pop();
                if (stk.empty()) {
                    stk.push(i);
                } else {
                    maxans = max(maxans, i - stk.top());
                }
            }
        }
        return maxans;
    }
};



class Solution {
public:
    vector<int> constructArray(int n, int k) {
        vector<int> answer;
        for (int i = 1; i < n - k; ++i) {
            answer.push_back(i);
        }
        for (int i = n - k, j = n; i <= j; ++i, --j) {
            answer.push_back(i);
            if (i != j) {
                answer.push_back(j);
            }
        }
        return answer;
    }
};



int main()
{
    return 0;
}