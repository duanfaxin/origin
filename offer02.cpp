#include<iostream>
#include<vector>
using namespace std;


class Solution {
public:
    int search(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size();
        int count = 0;
        while(right>left)
        {
            int mid = (left + right)/2;
            if(nums[mid] == target)
            {
                int n = mid+1;
                while(mid>=0&&nums[mid--] == target)count++;
                while(n<nums.size()&&nums[n++] == target)count++;
                return count;
            }else if(nums[mid]>target) right = mid;
            else left = mid + 1;
        }
        return count;
    }
};

int main()
{

    return 0;
}