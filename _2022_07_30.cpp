#include<iostream>

 //字符串哈希
using namespace std;

typedef unsigned long long ULL;
//用unsigned long long  超出六十四为相当于取模


const int N = 100010,p = 131; //规定这样不会有哈希冲突
int n,m;
char str[N];

ULL h[N],P[N];

ULL get(int l,int r) //获取区间的哈希值
{
    return h[r]-h[l-1]*P[r-l+1];
}

int main()
{
    scanf("%d%d",&n,&m);
    scanf("%s",str+1);
    P[0] = 1;
    
    for(int i = 1;i<=n;i++)
    {
        P[i] = P[i-1]*p;  //保存  为计算哈希公式做准备
        h[i] = h[i-1]*p+str[i]; //计算前缀哈希值
    }
    while(m--)
    {
        int l1,r1,l2,r2;
        scanf("%d%d%d%d",&l1,&r1,&l2,&r2);
        if(get(l1,r1) == get(l2,r2))
        {
            cout<<"Yes"<<endl;
        }else
        {
            puts("No");
        }
    }
    
    
    return 0;
}