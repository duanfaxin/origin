#include<iostream>

#include<stack>

#include<vector>

using namespace std;

class Solution {
public:
    int maxChunksToSorted(vector<int>& arr) {
        stack<int> st;
        bool f = false;
        for(int i = 0;i<arr.size();i++)
        {
            if(st.empty()||arr[i]>=st.top())
            {
                st.emplace(arr[i]);
            }else
            {
                int minn = st.top();
                  while(!st.empty()&&arr[i]<st.top())
                st.pop();
                st.emplace(minn);
           
            }
          
        }
        return st.size();
    }
};


int main()
{
    return 0;
}