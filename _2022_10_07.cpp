#include<iostream>
#include<vector>
#include<stack>
using namespace std;

class Solution {
public:
    int maxAscendingSum(vector<int>& nums) {
        stack<int> st;
        int maxx = 0;
        int t = 0;
        for(int i = 0;i<nums.size();i++)
        {
            if(nums[i]>t)
            {
                t = nums[i];
                maxx +=t;
            }else
            {
                if(st.empty()||maxx>st.top())
                {
                    st.push(maxx);
                }
                maxx = nums[i];
                t = nums[i];
            }
        }
        if(st.empty()||maxx>st.top())
        return maxx;
        return st.top();
    }
};

int main()
{
    return 0;//备注
}