#include<bits/stdc++.h>
using namespace std;

  struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };
class Solution {
public:
    unordered_map<int,int> mp;
    TreeNode* _buildTree(vector<int>& preorder,int pl,int pr,vector<int>& inorder,int il,int ir)
    {
        if(pl>pr)
        return nullptr;
        TreeNode* hh = new TreeNode(preorder[pl]);
        int n = mp[preorder[pl]]; //头节点的位置
        int p = n - il ;
        hh->left = _buildTree(preorder,pl+1,pl+p,inorder,il,n-1);
        hh->right = _buildTree(preorder,pl+p+1,pr,inorder,n+1,ir);
        return hh;
    }
    
    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        for(int i = 0;i<inorder.size();i++)
        {
            mp[inorder[i]] = i; //记录位置
        }
        return _buildTree(preorder,0,preorder.size()-1,inorder,0,inorder.size()-1);
    }
};