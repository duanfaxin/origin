#include<iostream>
#include<bits/stdc++.h>

using namespace std;

class Solution {
public:
    // bool com(vector<int> a,vector<int> b)
    // {
    //     return a[1]>b[1];
    // }
    int maximumUnits(vector<vector<int>>& boxTypes, int truckSize) {
        
        sort(boxTypes.begin(),boxTypes.end(),[&](vector<int> &a,vector<int> &b){
            return a[1]>b[1];
        });
        int count = 0;
        int ans = 0;
        while(count<boxTypes.size()&&truckSize>0)
        {
            if(boxTypes[count][0]<truckSize)
            {
                truckSize-=boxTypes[count][0];
                ans+=boxTypes[count][1]*boxTypes[count][0];
            }else
            {
                 ans+=truckSize*boxTypes[count][1];
                 break;
            }
            count++;
        }
        return ans;
    }
};


int main()
{
    return 0;
}