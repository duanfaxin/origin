#include<iostream>
using namespace std;
//tire 树
const int N = 100010,M = 31*N;

int son[M][2];
int a[N];
int idx;
void insert(int x)
{
    int q = 0;
    for(int i = 30;~i;i--)
    {
        int u = x>>i&1;
        if(!son[q][u])son[q][u] = ++idx;
        q = son[q][u];
    }
}

int query(int x)
{
    int res = 0;//保存结果
    int p = 0;
    for(int i = 30;~i;i--)
    {
        int u = x>>i&1;
        if(son[p][!u])
        {
            res = res*2+!u;
            p = son[p][!u];
        }else
        {
            res = res*2+u;
           p = son[p][u];
        }
    }
    return res;
}

int main()
{
    int n;
    cin>>n;
    for(int i = 0;i<n;i++)cin>>a[i];
    
    int res = 0;
   // int maxx = 0;
    for(int i = 0;i<n;i++)
    {
        insert(a[i]);
        int q = query(a[i]);
        res=max(res,q^a[i]);
    }
    cout<<res;
    return 0;
}