#include<iostream>
#include<set>  
using namespace std;

// 差分  
int a,b,c,d;

const int N  = 10010;

int height[N];

int main()
{
    
    cin>>a>>b>>c>>d;
    
    set<pair<int,int>> st;
    
    height[1] = c;//差分的思想 每个牛初始都是c  到后来要用前缀和来总和
    for(int i= 0;i<d;i++)
    {
        int n,m;
        cin>>n>>m;
        if(n>m)
        swap(n,m);
        if(st.count({n,m})==0)
        {

             st.insert({n,m});
            height[n+1]--;//n+1到m-1身高全部减一
            height[m]++;
        }
    }
    for(int i = 1;i<=a;i++)
    {
        height[i] += height[i-1];
        cout<<height[i]<<endl;
    }
    return 0;
}