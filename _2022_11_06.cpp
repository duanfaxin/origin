#include<iostream>
#include<string>
using namespace std;


class Solution {
public:
    string interpret(string command) {
        int len = command.size();
        string ans = "";
        for(int i = 0;i<len;i++)
        {
            if(command[i] == 'G')
            {
                ans+="G";
            }else if((i+1<len)&&command[i+1] == 'a')
            {
                i = i+3;
                ans+="al";
            }else
            {
                i+=1;
                ans+="o";
            }
        }
        return ans;
    }
};

int main()
{
    return 0;
}