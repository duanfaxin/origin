#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
const int N = 1010;
vector<vector<int>> vt(N,vector<int>(2));
int main()
{
    int n,m;
    cin>>n>>m;
    for(int i = 0;i<n;i++)
        cin>>vt[i][0]>>vt[i][1];
    sort(vt.begin(),vt.end());
    int cv=0,cw=0;
    vector<vector<int>> b;
    for(int i = 0;i<n;i++)
    {
        if(cv+vt[i][0]<=m)
        {
            b.push_back(vt[i]);
            cv+=vt[i][0];
            cw+=vt[i][1];
        }else
        {
            int t = b.size()-1;
            if(b[t][1]>=vt[i][1])
            continue;
            else
            {
                int tw = cw;
                int tv = cv;
                while(tv+vt[i][0]>m)
                {
                    tv-=b[t][0];
                    tw-=b[t][1];
                    t--;
                }
                tv+=vt[i][0];
                tw+=vt[i][1];
                if(tw>cw)
                {
                    while(t<b.size()-1)
                    {
                        b.pop_back();
                        t++;
                    }
                    b.push_back(vt[i][0]);
                    cw = tw;
                    cv = tv;
                }
            }
        }
    }
    cout<<cv<<" "<<cw<<endl;
    return 0;
}
