#include<iostream>
#include<vector>
using namespace std;

const int N = 20;

bool st[N];
        
int n;
vector<int> temp;

void dfs(int n)
{
    if(temp.size() == n)
    {
        for(int i = 0;i<n;i++)
            cout<<temp[i]<<" ";
            cout<<endl;
            return ;
    }
    for(int i = 1;i<=n;i++)
    {
        if(!st[i])
        {
            st[i] = true;
            temp.push_back(i);
            dfs(n);
            st[i] = false;
            temp.pop_back();
        }
    }
}

int main()
{
    cin>>n;
    dfs(n);
    return 0;
}