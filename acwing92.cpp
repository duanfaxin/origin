#include <iostream>
using namespace std;
typedef long long ll;

int n;
int a[20];
bool vis[20];

// 一共tar个坑，当前枚举到第pos个坑
void dfs(int pos, int tar) {
    if (pos == tar + 1) {
        for (int i = 1; i <= tar; i ++ ) cout << a[i] << " ";
        cout << endl;
        return ;
    }

    // 选数填坑，选择的数范围是1～n
    for (int i = 1; i <= n; i ++) {
        if (!vis[i]) {
            vis[i] = true; a[pos] = i;
            dfs (pos + 1, tar);
            vis[i] = false;
        }
    }
}

int main() {
    cout << endl; // 不取
    cin >> n;
    for (int i = 1; i <= n; i ++ )
        dfs(1, i);
    return 0;
}
