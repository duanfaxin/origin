#include<bits/stdc++.h>
using namespace std;


class Solution {
public:
    bool isContinuous( vector<int> numbers ) {
        if(numbers.empty())
        return false;
        sort(numbers.begin(),numbers.end());
        int k = 0;
        while(numbers[k] == 0)k++;
        for(int i = k+1;i<numbers.size();i++)
        if(numbers[i]==numbers[i-1])return false;
        //排除法  排除了相等的例子  
        return numbers.back()-numbers[k]<=4;
    }
};

int main()
{
    return 0;
}