#include<bits/stdc++.h>

class Solution {
public:
    bool isugly(int n)
    {
        while(n!=2||n!=3||n!=5)
        {
            if(n%2 ==0)
            n/=2;
            else if(n%3 == 0)
            n/=3;
            else if(n%5 ==0)
            n/=5;
            else
            return false;
            if(n<2)break;
        }
        return true;
    }
    int nthUglyNumber(int n) {
        if(n == 1)return 1;
        int count = 1;
        for(int i = 2;;i++)
        {
            if(isugly(i))count++;
            else continue;
            if(count == n)return i;
        }
        return count;
    }
};