#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;

//开两个数组代表节点的位置和写一个节点的位置


const int N = 100010;

typedef pair<int,int> PII;
int h1,h2,n;
int v[N],ne[N]; //v[N] 下标代表的是节点的地址   ne是代表指针

int main()
{
    scanf("%d%d%d",&h1,&h2,&n);
    while(n--)
    {
        int addr,val,naddr;
        scanf("%d%d%d",&addr,&val,&naddr);
        v[addr] = val;
        ne[addr] = naddr; //指向的地址
    }
    vector<PII> a,b; //两个链表
   
    for(int i = h1;i!=-1;i = ne[i])a.push_back({i,v[i]});
    for(int i = h2;i!=-1;i = ne[i])b.push_back({i,v[i]});
     if(a.size()<b.size())swap(a,b);
    vector<PII> c;
    //链表合并
    for(int i = 0,j = b.size()-1;i<a.size();i+=2,j--)
    {
        c.push_back(a[i]);
        if(i+1<a.size())c.push_back(a[i+1]);
        if(j>=0)c.push_back(b[j]);
    }
    for(int i = 0;i<c.size();i++)
    {
        printf("%05d %d ",c[i].first,c[i].second);
        if(i+1<c.size())printf("%05d\n",c[i+1].first);
        else puts("-1");
    }
    
}