#include<iostream>
#include<vector>
#include<queue>


using namespace std;


class Solution {
public:
    int shortestBridge(vector<vector<int>>& grid) {
        //首先先把最外层的一层变为-1
        //
        int n = grid.size();
        queue<pair<int,int>> que;//队列
        int dx[4] = {0,1,0,-1};
        int dy[4] = {1,0,-1,0};
        //方向
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<n;j++)
            {
                if(grid[i][j] == 1)
                {
                    que.push({x,y});
                    grid[x][y] = -1;
                    while(!que.empty())
                    {
                        auto it = que.front();
                        for(int a = 0;a<4;i++)
                        {
                            int nx = x + dx[a];
                            int ny = y + dy[a];
                            if(nx>=0&&ny>=0&&nx<n&&ny<n&&grid[nx][ny]==0)
                            {
                                que.push({nx,ny});
                                grid[nx][ny] = -1;
                            }
                        }
                    }
                }
            }
        }
    }
};

class Solution {
public:
    int shortestBridge(vector<vector<int>>& grid) {
        int n = grid.size();
        vector<vector<int>> dirs = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
        vector<pair<int, int>> island;
        queue<pair<int, int>> qu;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    qu.emplace(i, j);
                    grid[i][j] = -1;
                    while (!qu.empty()) {
                        auto [x, y] = qu.front();
                        qu.pop();
                        island.emplace_back(x, y);
                        for (int k = 0; k < 4; k++) {
                            int nx = x + dirs[k][0];
                            int ny = y + dirs[k][1];
                            if (nx >= 0 && ny >= 0 && nx < n && ny < n && grid[nx][ny] == 1) {
                                qu.emplace(nx, ny);
                                grid[nx][ny] = -1;
                            }
                        }
                    }
                    for (auto &&[x, y] : island) {
                        qu.emplace(x, y);
                    }
                    int step = 0;
                    while (!qu.empty()) {
                        int sz = qu.size();
                        for (int i = 0; i < sz; i++) {
                            auto [x, y] = qu.front();
                            qu.pop();
                            for (int k = 0; k < 4; k++) {
                                int nx = x + dirs[k][0];
                                int ny = y + dirs[k][1];
                                if (nx >= 0 && ny >= 0 && nx < n && ny < n) {
                                    if (grid[nx][ny] == 0) {
                                        qu.emplace(nx, ny);
                                        grid[nx][ny] = -1;
                                    } else if (grid[nx][ny] == 1) {
                                        return step;
                                    }
                                }
                            }
                        }
                        step++;
                    }
                }
            }
        }
        return 0;
    }
};
// class Solution {
// public:
//     int shortestBridge(vector<vector<int>>& grid) {
//         //首先先把最外层的一层变为-1
//         //
//         int n = grid.size();
//         queue<pair<int,int>> que;//队列
//         int dx[4] = {0,1,0,-1};
//         int dy[4] = {1,0,-1,0};
//         //方向
//         for(int i = 0;i<n;i++)
//         {
//             for(int j = 0;j<n;j++)
//             {
//                 if(grid[i][j] == 1)
//                 {
//                     que.push({i,j});
//                     grid[i][j] = -1;
//                     while(!que.empty())
//                     {
//                         auto it = que.front();
//                         int x = it.first;
//                         int y = it.second;
//                         que.pop();
//                         for(int a = 0;a<4;a++)
//                         {
//                             int nx = x + dx[a];
//                             int ny = y + dy[a];
//                             if(nx>=0&&ny>=0&&nx<n&&ny<n&&grid[nx][ny]==1)
//                             {
//                                 que.push({nx,ny});
//                                 grid[nx][ny] = -1;
//                             }
//                         }
//                     }
//                 }
//                 int step = 0;
//                 while(!que.empty())
//                 {
//                     int sz = que.size();
//                     for(int e = 0;e<sz;e++)
//                     {
//                         auto it = que.front();
//                         que.pop();
//                         int x = it.first;
//                         int y = it.second;
//                          for(int a = 0;a<4;a++)
//                         {
//                             int nx = x + dx[a];
//                             int ny = y + dy[a];
//                             if(nx>=0&&ny>=0&&nx<n&&ny<n){
//                                if(grid[nx][ny]==0) {
//                                 que.push({nx,ny});
//                                 grid[nx][ny] = -1;
//                             }else if(grid[nx][ny] == 1)
//                             return step;
//                             }
                            
//                         }
//                     }
//                     step++;
//                 }
//             }
//         }
//         return 0;
//     }
// };

class Solution {
public:
    int shortestBridge(vector<vector<int>>& grid) {
        int n = grid.size();
        vector<vector<int>> dirs = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
        vector<pair<int, int>> island;
        queue<pair<int, int>> qu;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    qu.emplace(i, j);
                    grid[i][j] = -1;
                    while (!qu.empty()) {
                        auto [x, y] = qu.front();
                        qu.pop();
                        island.emplace_back(x, y);
                        for (int k = 0; k < 4; k++) {
                            int nx = x + dirs[k][0];
                            int ny = y + dirs[k][1];
                            if (nx >= 0 && ny >= 0 && nx < n && ny < n && grid[nx][ny] == 1) {
                                qu.emplace(nx, ny);
                                grid[nx][ny] = -1;
                            }
                        }
                    }
                    for (auto &&[x, y] : island) {
                        qu.emplace(x, y);
                    }
                    int step = 0;
                    while (!qu.empty()) {
                        int sz = qu.size();
                        for (int i = 0; i < sz; i++) {
                            auto [x, y] = qu.front();
                            qu.pop();
                            for (int k = 0; k < 4; k++) {
                                int nx = x + dirs[k][0];
                                int ny = y + dirs[k][1];
                                if (nx >= 0 && ny >= 0 && nx < n && ny < n) {
                                    if (grid[nx][ny] == 0) {
                                        qu.emplace(nx, ny);
                                        grid[nx][ny] = -1;
                                    } else if (grid[nx][ny] == 1) {
                                        return step;
                                    }
                                }
                            }
                        }
                        step++;
                    }
                }
            }
        }
        return 0;
    }
};

int main()
{
    return 0;
}