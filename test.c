#include"game.h"
#include<limits.h>

void menu()
{
	printf("**********************\n");
	printf("******0.exit**********\n");
	printf("******1.paly**********\n");
	printf("**********************\n");
}
void game()
{
	printf("开始玩儿游戏\n");
	char board[ROW][COL] = { 0 };
	InitBoard(board, ROW, COL);
	ShowBoard(board, ROW, COL);
	char ret;
	while (1)
	{
		PlayerMove(board, ROW, COL);
		//每走一步 都得判断  输赢
		ret = IsWin(board, ROW, COL);
		if (ret != ' ')
		{
			break;
		}
		ShowBoard(board, ROW, COL);
		ComputerMove(board, ROW, COL);
		ret = IsWin(board, ROW, COL);
		if (ret != ' ')
		{
			break;
		}
		ShowBoard(board, ROW, COL);
	}
	if (ret == 'X')
	{
		printf("玩家赢\n");
	}
	else if (ret == 'O')
	{
		printf("电脑赢\n");
	}
	else if (ret == 'Q')
	{
		printf("平局\n");
	}
}
int main()
{
	//随机种子
	srand((unsigned)time(NULL));
	int input;
	do
	{
		menu();
		printf("请输入你的操作：\n");
		scanf_s("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			break;
		}
	} while (input != 0);

	return 0;
}

