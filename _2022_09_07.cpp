#include<iostream>
#include<bits/stdc++.h>
using namespace std;


class Solution {
public:
    string reorderSpaces(string text) {
        int n = text.size();
        vector<string> ans;
        int index = 0;
        string t = "";
        int count = 0;
        while(index<text.size())
        {
            if(index<n&&text[index] == ' ')
            {
                            count++;
                            index++;
            }
            while(index<n&&text[index]!=' ')
            {  
                t+=text[index];
                index++;
            }
            if((index == n&&text[index-1]!=' ')||(text[index]==' '&&text[index-1]!=' '))
            {
                ans.push_back(t);
                t = "";
            }
        }
       // ans.pop_back();
          string ret = "";
       if(ans.size() == 1)
       {
           ret = ans[0];
           for(int i = 0;i<count;i++)
           ret+=' ';
           return ret;
       }
        int a = count/(ans.size()-1);
        int b = count%(ans.size()-1);

     
        for(int z = 0;z<ans.size();z++)
        {
            ret+=ans[z];
            if(z == ans.size()-1)
            break;
            for(int i = 0;i<a;i++)
            ret+=' ';
        }
        for(int i = 0;i<b;i++)
        ret+=' ';
        //cout<<count<<" "<<ans.size()<<" "<<a<<" "<<b<<endl;
        return ret;
    }
};


int main()
{
    return 0;
}