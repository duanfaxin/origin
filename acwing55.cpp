#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
const int INT_MIN = -100001;
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int n = nums.size();
          int maxx = INT_MIN;
        vector<vector<int> > ans(n,vector<int>(n));
        for(int i = 0;i<n;i++)
        {
             ans[i][i] = nums[i];
             maxx = max(nums[i],maxx);
        }
        for(int i = 0;i<n;i++)
        {
            for(int j = i+1;j<n;j++)
            {
                ans[i][j] = ans[i][j-1]+ans[j][j];
                maxx = max(ans[i][j],maxx);
            }
        }
            return maxx;
    }
};
int main()
{ 
    return 0;
}