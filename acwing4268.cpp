#include<iostream>

using namespace std;

bool isu(int n) //判断是否素数
{
    if(n <= 1)
    return false;
    for(int i = 2;i*i<=n;i++)
    {
        if(n%i == 0)
        return false;
    }
    return true;
}

int main()
{
    int n;
    cin>>n;
    if(isu(n)&&(isu(n+6)||isu(n-6)))
    {
        cout<<"Yes"<<endl;
        if(isu(n-6))
        {
            cout<<n-6;
        }
        else
        {
              cout<<n+6;
        }
      
    }else
    
    {
        cout<<"No"<<endl;
        for(int i = n+1;;i++)
        {
            if(isu(i)&&(isu(i+6)||isu(i-6)))
            {
                cout<<i;
                return 0;
            }
        }
    }
    return 0;
}