#include<iostream>
#include<pthread.h> 

using namespace std;
pthread_mutex_t mutex;
//懒汉模式 

class solo{
    static solo* getinstrance();
   
private:
    solo(){}
    solo(solo* s){} //私有化拷贝构造
    solo operator=(solo* s){}
    
   static solo* sol;
    
};

   solo* solo::sol = nullptr;
    solo* solo::getinstrance()
    {
        if(sol == nullptr)
        { 
             pthread_mutex_lock(&mutex);
             if(sol == nullptr)
            sol = new solo();
        }
        return sol;
    }

int main()
{
    pthread_mutex_init(&mutex,nullptr);

    return 0;
}