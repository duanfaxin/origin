#include<iostream>

#include<vector>
using  namespace std;
#define INT_MAX 10000000
class Solution {
public:
    int mininve(vector<int> &arr,int l,int r)
    {
        int minn = INT_MAX;
        for(int i = l;i<=r;i++)
        {
            minn = minn>arr[i]?arr[i]:minn;
        }
        return minn;
    }
    int sumSubarrayMins(vector<int>& arr) {
        const int N = 1000000007;
        int count = 0;
        for(int i = 1;i<arr.size();i++)
        {
            for(int j = 0;j+i<arr.size();j+=i)
            {
                count=(count+mininve(arr,j,j+i))%N;
            }
        }
        return count;
    }
};

int main()
{
    return 0;
}