#include<iostream>
#include<string>

using namespace std;

class Solution {
public:
    string mergeAlternately(string word1, string word2) {
        int s1 = 0,s2 = 0;
        bool f = false;
        string ans;
        while(s1<word1.size()&&s2<word2.size())
        {
            if(!f)
            {
                f = !f;
                ans.push_back(word1[s1++]);
            }else{
                ans.push_back(word2[s2++]);
                f = !f;
            }
        }
        if(s1 == word1.size())
        {
            while(s2<word2.size())
            {
                 ans.push_back(word2[s2++]);
            }
        }else{
            while(s1<word1.size())
            {
                 ans.push_back(word1[s1++]);
            }
        }
        return ans;
    }
};

int main()
{
    return 0;
}