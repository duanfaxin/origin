#include<iostream>
#include<bits/stdc++.h>

using namespace std;


class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    vector<vector<int>> permutation(vector<int>& nums) {
       sort(nums.begin(),nums.end());
       path.resize(nums.size());
       dfs(nums,0,0,0);
       return ans;
    }
    void dfs(vector<int> nums,int u,int start,int state)
    {
        if(u == nums.size())
        {
            ans.push_back(path);
            return ;
        }
        if(!u||nums[u]!=nums[u-1])start = 0;//前一个数字不与下一个数字相等
        for(int i = start;i<nums.size();i++)
        {
            if((state>>i&1)==0)
            {
                path[i] = nums[u]; //i没有被填充 
                dfs(nums,u+1,i+1,state+(1<<i));
            }
        }
    }
};
class Solution {
public:
    bool halvesAreAlike(string s) {
        unordered_set<char> st({'a','e','i','o','u','A','E','I','O','U'});
        int len = s.size()/2;
        int count = 0;
        for(int i = 0;i<len;i++)
        {
            if(st.count(s[i])!=0)
            count++;
        }
        for(int i = len;i<s.size();i++)
        {
            if(st.count(s[i])!=0)
            count--;
        }
        return !count;
    }
};


int main()
{
    return 0;
}