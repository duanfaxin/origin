#include<iostream>
#include<vector>
using namespace std;

class Solution {
public:
    vector<vector<int> > findContinuousSequence(int sum) {
        //有单调性  i增大  j增大
       int i = 1,j = 2;
       vector<vector<int>> ans;
       vector<int> temp;
       for(i,j;j<=sum/2+1,i<j;)
       {
           int count = (i+j)*(j-i+1)/2;
           if(sum == count)
           {
               for(int index = i;index<=j;index++)
               temp.push_back(index);
               ans.push_back(temp);
               temp.clear();
               i++,j++;
           }else if(count<sum) j++;
           else i++;
       }
       return ans;
    }
};


// class Solution {
// public:
//     vector<vector<int> > findContinuousSequence(int sum) {
//         vector<vector<int>> ans;
//         for(int i = 1;i<=sum/2+1;i++)
//         {
//             vector<int> temp;
//             for(int j = i+1;j<=sum/2+1;j++)
//             {
//                 int count = ((i+j)*(j-i+1));
//                 if(count/2 == sum)
//                 {
//                     for(int a = i;a<=j;a++)temp.push_back(a);
//                     ans.push_back(temp);
//                     break;
//                 }
//             }
//         }
//         return ans;
//     }
// };



int main()
{
    return 0;
}