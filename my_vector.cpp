#include<iostream>
#include<math.h>
#include <stdio.h>
using namespace std;

namespace VECTOR{
template<class T>
class iterator{
    typedef T  value_type;
    typedef T* point;
public:
    iterator* operator++();

};

template<class T>
class my_vector{
    typedef T value_type;
    typedef T* iterator;
public:
    my_vector()
    {
        capacity = 5;
        arr = new T[capacity];//数组初始容量  
        first = end = 0; 
    }
    T operator[]();
    my_vector(my_vector& temp);
    my_vector operator=();
    size_t size();
    void push_back(T val);
    void push_front(T val);
    void pop_back();
    void pop_front();
    void _insert(int pos,T val);
    iterator begin();
    iterator end();

    
private:
    T* arr;
    size_t first;
    size_t end;
    size_t capacity;
    iterator pos;//位置指针
};
template<class T>
void my_vector<T>::_insert(int pos,T val){
    assert(pos<=end)
    if(end>=capacity)
    {
        capacity = 2*capacity;
         T* arr_temp = new [capacity];
        for(int i = 0;i<=end;i++)
        arr_temp[i] = arr[i];
        delete[] arr;
        arr = arr_temp; 
      }
      for(int i = end;i>=pos;i++)
        arr[i+1] = arr[i];
        arr[pos] = val;
}

template<class T>
void my_vector<T>::push_back(T val)
{
    _insert(end,val);
}
template<class T>
void my_vector<T>::push_front(T val)
{
    _insert(first,val);
}
}



int main()
{

    int n = pow(3,2);
    cout<<n;
    return 0;
}