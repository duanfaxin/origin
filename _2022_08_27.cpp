#include<iostream>
#include<bits/stdc++.h>
using namespace std;
class Solution {
public:
    vector<int> temp;
    int count = 0;
    int merge_sort(vector<int>& nums,vector<int> temp,int l,int r)
    {
        if(l ==r) return 0;
        int mid = l + r >>1;
       int lc =  merge_sort(nums,temp,l,mid);
       int rc =  merge_sort(nums,temp,mid+1,r);
        int k = 0,cut = 0;
        int i = l,j = mid+1;
        while(i<=mid&&j<=r)
        {
            if(nums[i]>nums[j])
            {
                temp[k++] = nums[j++];
                cut = cut+mid-i+1;
            }else
            temp[k++] = nums[i++];
        }
        while(i<=mid)
        {
            temp[k++] = nums[i++];
        }
        while(j<=r)
        {
            temp[k++] = nums[j++];
        }
        for(int a = l;a<=r;a++)
        {
            nums[a] = temp[a-1];
        }
        return cut+lc+rc;
    }
    int reversePairs(vector<int>& nums) {
        temp.resize(nums.size());
        return merge_sort(nums,temp,0,nums.size()-1);
    }
};


int main()
{
    return 0;
}