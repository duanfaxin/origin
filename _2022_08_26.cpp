#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */

class Solution {
public:
    bool isUnique(string astr) {
        int d[26];
        memset(d,0,sizeof(d));
        for(auto e:astr)
        {
            if(++d[e-'a']>=2)
            {
                return false;
            }
        }
        return true;
    }
};
struct TreeNode
{
    TreeNode* right;
    TreeNode* left;
    int val;
    /* data */
};

class Solution {
public:
    vector<string> binaryTreePaths(TreeNode* root) {
        vector<string> ans;
        if(root == nullptr) return ans;

        dfs(root,ans,"");
        return ans;

    }

    void dfs(TreeNode* root,vector<string>& ans,string res)
    {
        res+=to_string(root->val); //保存路径
        if(root->left==nullptr&&root->right == nullptr)
        {
            ans.push_back(res);
            return ;
        }
        if(root->left) dfs(root->left,ans,res+"->");
        if(root->right) dfs(root->right,ans,res+"->");
    }
};
class Solution {
public:
    int maxProduct(vector<int>& nums) {
        sort(nums.begin(),nums.end());
        return (nums[nums.size()-1]-1)*(nums[nums.size()-2]-1);
    }
};

int main()
{
    return 0;
}