#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <wait.h>

int main()
{
    pid_t ret = fork();
    if(ret<0)
    exit(1);
    else if(ret == 0)
    {
     printf("this is son %d\n",getpid());
    //char* argv[] = {"ls","-al","./"};
     //execlp("ls","","NULL");
     
     //execlp("pwd","",NULL);
     
    }else
    {
     wait(NULL);
     printf("this is father %d\n",getpid());
    }
    return 0;
}


/*
int main()
{
    int n = open("a.txt",O_RDWR | O_CREAT,0);
    pid_t ret = fork();
    if(ret<0)
    exit(1);
    else if(ret == 0)
    {
       // char* p = "abcd";
       char p[] = "abcd";
        write(n,p,4);
    }else
    {
       // char* p = "abcd";
       char p[] = "abcd";
        write(n,p,4);
        char buf[100];
        read(n,buf,7);
        puts(buf);
    }
    return 0;
}

int main()
{
    //int x = 100;
    pid_t ret = fork();
    if(ret<0)
    exit(1);
    else if(ret == 0)
    {
       // printf("%d\n",x);
      //  puts("");
    }else
    {
      //  printf("%d\n",x);
      //  puts("");
    }
    return 0;
}*/