#include<iostream>
#include<vector>
#include<string>
#include<algorithm>

using namespace std;

class Solution {
public:
    string orderlyQueue(string s, int k) {
        if(k == 1)
        {
           // s+=s;
            string ans(s);
            int len = s.size();
            for(int i = 0;i<len;i++)
            {
                ans = min(ans,string(s.begin()+i,s.end())+string(s.begin(),s.begin()+i));
            }
            return { ans.cbegin(),ans.cend() };
        }else 
        {
            sort(s.begin(),s.end());
        }
        return s;
    }
};


int main()
{
    return 0;
}