#include<iostream>
#include<vector>
#include<string>
#include<unordered_set>
using namespace std;
class Solution {
public:
    int countConsistentStrings(string allowed, vector<string>& words) {
        unordered_set<char> mp;
        for(auto &e:allowed)
        {
            mp.insert(e);
        }
        int count = words.size();;
        for(int i = 0;i<words.size();i++)
        {
            for(auto &e:words[i])
            {
                if(mp.count(e) == 0)
                {
                    count--;
                    break;
                }
            }
        }
        return count;
    }
};



int main()
{
    return 0;
}