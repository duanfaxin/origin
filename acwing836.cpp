#include<bits/stdc++.h>

using namespace std;

const int N = 100010;

int p[N],ssize[N];


int find(int x) //找到祖宗节点
{
    if(x != p[x])p[x] = find(p[x]); //压缩路径
    return p[x];
}


int main()
{
    int n,m;
    cin>>n>>m;
    for(int i = 0;i<n;i++)
    {
        p[i] = i;
        ssize[i] = 1;
    }
    while(m--)
    {
        char op[5];
        scanf("%s",op);
        if(op[1] == '1')
        {
            int a,b;
            scanf("%d %d",&a,&b);
            if(find(a) == find(b))printf("Yes\n");
            else printf("No\n");
        }else if(op[1] == '2')
        {
            int a;
            scanf("%d",&a);
            printf("%d\n",ssize[find(a)]);
        }else
        {
            int a,b;
            scanf("%d %d",&a,&b);
            if(find(a) == find(b))continue;
            ssize[find(a)]+=ssize[find(b)];
            p[find(b)] = find(a);
        }
        
    }
    
    
    return 0;
}