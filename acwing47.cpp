#include<iostream>
#include<bits/stdc++.h>
using namespace std;
 
  struct TreeNode {
      int val;
     TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
  };
 
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> temp;
    void dfs(TreeNode* root, int sum)
    {
        if(!root)
        return ;
         temp.push_back(root->val);
         sum-=root->val;
        if(!root->left&&!root->right&&sum == 0)
        {
            ans.push_back(temp);
        }
          dfs(root->left,sum);
        dfs(root->right,sum);
        temp.pop_back();
    }
    vector<vector<int>> findPath(TreeNode* root, int sum) {
        dfs(root,sum);
        return ans;
    }
};


int main()
{
    return 0;
}