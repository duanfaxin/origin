#include<iostream>
#include<vector>
#include<queue>
using namespace std;
class Solution {
public:
    vector<string> letterCasePermutation(string s) {
        queue<string> que;
        for(auto &e:s)
        {
            if(e>='a'&&e<='z'||e>='A'&&e<='Z')
            {
                if(que.empty())
                {
                string a = "";
                char t;
                if(e<='Z')
                t = e + 32;
                else
                t = e - 32;
                string te = a + t;
                que.push(te);
                te = a + e;
                que.push(te);
                }else{
                     int s = que.size();
                for(int i = 0;i<s;i++)
                {
                auto a = que.front();
                que.pop();
                char t;
                if(e<='Z')
                t = e + 32;
                else
                t = e - 32;
                string te = a+t;
                que.push(te);
                te = a + e;
                que.push(te);
                }
                } 
            }else{
                if(que.empty())
                {
                    string a = "";
                    a+=e;
                    que.push(a);
                }else
                {
                    int s = que.size();
                for(int i = 0;i<s;i++)
                {
                    auto a = que.front();
                    que.pop();
                    a+=e;
                    que.push(a);
                }
                }
                
            }
        } 
        vector<string> ans;
        while(!que.empty())
        {
            ans.push_back(que.front());
            que.pop();
        }
        return ans;
    }
};

int  main()
{
    return 0;
}