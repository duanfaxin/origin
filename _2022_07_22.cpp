#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
using namespace std;
class Solution {
public:
    //自定义 比较函数
     static bool comp(string &a,string &b)
    {
        if(a+b>=b+a)return false;
        return true;
      // return a+b<b+a;
    }
    string minNumber(vector<int>& nums) {
        //可以利用字符串的排序 int->string 
        vector<string> st;
        for(auto &e:nums)
        st.push_back(to_string(e));
        sort(st.begin(),st.end(),comp);
        string ans;
        for(auto &e:st)
        ans+=e;
        return ans;
    }
};
int main()
{
    return 0;
}