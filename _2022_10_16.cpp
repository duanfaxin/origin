#include<iostream>

#include<bits/stdc++.h>

using namespace std;

class Solution {
public:
    int findFa(int x, vector<int>& fa) {
        return fa[x] < 0 ? x : fa[x] = findFa(fa[x], fa);
    }

    void unit(int x, int y, vector<int>& fa) {
        x = findFa(x, fa);
        y = findFa(y, fa);
        if (x == y) {
            return ;
        }
        if (fa[x] < fa[y]) {
            swap(x, y);
        }
        fa[x] += fa[y];
        fa[y] = x;
    }

    bool isconnect(int x, int y, vector<int>& fa) {
        x = findFa(x, fa);
        y = findFa(y, fa);
        return x == y;
    }

    bool possibleBipartition(int n, vector<vector<int>>& dislikes) {
        vector<int> fa(n + 1, -1);
        vector<vector<int>> g(n + 1);
        for (auto& p : dislikes) {
            g[p[0]].push_back(p[1]);
            g[p[1]].push_back(p[0]);
        }
        for (int i = 1; i <= n; ++i) {
            for (int j = 0; j < g[i].size(); ++j) {
                unit(g[i][0], g[i][j], fa);
                if (isconnect(i, g[i][j], fa)) {
                    return false;
                }
            }
        }
        return true;
    }
};
// Solution {
// public:
//     //并查集
//     int fa[4005];
//     int findr(int a)
//     {
//         if(a ==fa[a]) return a;
//         //压缩路径
//         return fa[a] = findr(fa[a]);//递归查找fa[a]
//     }
//     void merge(int a,int b)
//     {
//         fa[findr(a)] = findr(b);
//     }
//     bool possibleBipartition(int n, vector<vector<int>>& dislikes) {
//         for(int i = 0;i<=n*2;i++)
//             fa[i] = i;
//         for(auto &e:dislikes)
//         {
//             if(findr(e[0]) == findr(e[1])) return false;
//             merge(e[0],e[1]+n);
//             merge(e[0]+n,e[1]);
//         }
//         return true;
//     }
// };
int main()
{
    return 0;
}
