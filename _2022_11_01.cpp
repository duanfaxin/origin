#include<iostream>

#include<vector>
#include<string>

using namespace std;


class Solution {
public:
    bool arrayStringsAreEqual(vector<string>& word1, vector<string>& word2) {
        string s1 = "";
        string s2 = "";
        for(auto &e:word1)
        {
            s1+=e;
        }
        for(auto &e:word2)
        {
            s2+=e;
        }
        if(s1 == s2)
        return true;
        return false;
    }
};
int main()
{
    return 0;
}