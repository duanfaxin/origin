#include<iostream>
#include<unordered_map>
#include<string>
#include<vector>
using namespace std;
class Solution {
public:

    int dy[4] = {0,1,0,-1};
    int dx[4] = {1,0,-1,0};
    //i,j即是作为一开始的index还是递归下去寻找的基准
    bool dfs(vector<vector<char>>& matrix,int i,int j, string &str,int index)
    {
        if(matrix[i][j] != str[index])return false;
        if(index == str.size()-1) return true;
        char t = matrix[i][j];
        matrix[i][j] = '*';
        for(int a = 0;a<4;a++)
        {
            int ti = i+dx[a];
            int tj = j+dy[a];
            if(ti>=0&&ti<matrix.size()&&tj>=0&&tj<matrix[0].size())
            {
                if(dfs(matrix,ti,tj,str,index+1))
                return true;
            }
        }
        matrix[i][j] = t;
        return false;
    }
    bool hasPath(vector<vector<char>>& matrix, string &str) {
        int n = matrix.size();
        if(n == 0) return false;
        int m = matrix[0].size();
        
       if(n*m<str.size()) return false;
        for(int i = 0;i<n;i++)
        {
            for(int j = 0;j<m;j++)
            {
                if(dfs(matrix,i,j,str,0))
                {
                    return true;
                }
            }
        }
        return false;
    }
};

//class Solution {
// public:
//     bool hasPath(vector<vector<char>>& matrix, string str) {
//         for (int i = 0; i < matrix.size(); i ++ )
//             for (int j = 0; j < matrix[i].size(); j ++ )
//                 if (dfs(matrix, str, 0, i, j))
//                     return true;
//         return false;
//     }

//     bool dfs(vector<vector<char>> &matrix, string &str, int u, int x, int y) {
//         if (matrix[x][y] != str[u]) return false;
//         if (u == str.size() - 1) return true;
//         int dx[4] = {-1, 0, 1, 0}, dy[4] = {0, 1, 0, -1};
//         char t = matrix[x][y];
//         matrix[x][y] = '*';
//         for (int i = 0; i < 4; i ++ ) {
//             int a = x + dx[i], b = y + dy[i];
//             if (a >= 0 && a < matrix.size() && b >= 0 && b < matrix[a].size()) {
//                 if (dfs(matrix, str, u + 1, a, b)) return true;
//             }
//         }
//         matrix[x][y] = t;
//         return false;
//     }
// };




class Solution {
public:
    unordered_map<char,int> mp;
    void init()
    {
        //int i = 1;
        char t = 'a';
        for(int i = 0;i<26;i++)
        {
            mp[t+i] = i+1;
        }
    }
    int getLucky(string s, int k) {
        init();
        string temp = "";
        for(auto &e:s)
        {
            temp+=to_string(mp[e]);
        }
       // string ans;
         cout<<temp<<endl;
        int count = 0;
        for(int i = 0;i<k;i++)
        {
            for(auto &e:temp)
            {
                count+=e-'0';
            }
            temp = to_string(count);
            cout<<temp<<endl;
            if(i == k-1)
            return count;
            count = 0;
        }
        return -1;
    }
};


int main()
{
    return 0;
}